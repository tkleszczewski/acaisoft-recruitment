import * as actionTypes from './actionTypes';

import axios from 'axios';

function initFetchServersAsync() {
  return dispatch => {
    dispatch(initFetchServers());

    axios.get('/servers')
      .then(
        response => {
          const servers = response.data;
          dispatch(fetchServersResponseSuccess(servers));
        }
      )
      .catch(
        error => {
          dispatch(fetchServersResponseError())
        }
      );
  };
}

function initFetchServers() {
  return {
    type: actionTypes.FETCH_SERVERS_INIT,
  };
}

function fetchServersResponseSuccess(servers) {
  return {
    type: actionTypes.FETCH_SERVERS_RESPONSE_SUCCESS,
    payload: {
      servers,
    }
  };
}

function fetchServersResponseError() {
  return {
    type: actionTypes.FETCH_SERVERS_RESPONSE_ERROR,
  }
}

function initTurnOnServerAsync(serverId) {
  return dispatch => {
    dispatch(initTurnOnServer(serverId))

    axios.put(`servers/${serverId}/on`, {})
      .then(
        response => {
          const server = response.data
          dispatch(turnOnServerResponseSuccess(server))
        }
      )
      .catch(
        error => {
          dispatch(turnOnServerResponseError())
        }
      );
  }
}

function initTurnOnServer(serverId) {
  return {
    type: actionTypes.TURN_ON_SERVER_INIT,
    payload: {
      serverId,
    },
  };
}

function turnOnServerResponseSuccess(server) {
  return {
    type: actionTypes.TURN_ON_SERVER_RESPONSE_SUCCESS,
    payload: {
      server,
    },
  };
}

function turnOnServerResponseError() {
  return {
    type: actionTypes.TURN_ON_SERVER_RESPONSE_ERROR,
  };
}

function initTurnOffServerAsync(serverId) {
  return dispatch => {
    dispatch(initTurnOffServer(serverId));

    axios.put(`/servers/${serverId}/off`)
      .then(
        response => {
          const server = response.data
          dispatch(turnOffServerResponseSuccess(server));
        }
      )
      .catch(
        error => {
          dispatch(turnOffServerResponseError());
        }
      )
  }
}


function initTurnOffServer(serverId) {
  return {
    type: actionTypes.TURN_OFF_SERVER_INIT,
    payload: {
      serverId,
    },
  };
}

function turnOffServerResponseSuccess(server) {
  return {
    type: actionTypes.TURN_OFF_SERVER_RESPONSE_SUCCESS,
    payload: {
      server,
    },
  };
}

function turnOffServerResponseError() {
  return {
    type: actionTypes.TURN_OFF_SERVER_RESPONSE_ERROR,
  };
}

function initRebootServerAsync(serverId) {
  return dispatch => {
    dispatch(initRebootServer(serverId));

    axios.put(`/servers/${serverId}/reboot`)
      .then(
        response => {
          const server = response.data;
          dispatch(rebootServerResponseSuccess(server));
          pingServer(serverId);
        }
      )
      .catch(
        error => {
          dispatch(rebootServerResponseError());
        }
      );

    function pingServer(serverId) {
      axios.get(`/servers/${serverId}`)
        .then(
          response => {
            if (response.data.status === 'ONLINE') {
              dispatch(pingServerResponseOnline(response.data))
            } else {
              pingServer(serverId)
            }
          }
        );
    }
  }
}

function pingServerResponseOnline(server) {
  return {
    type: actionTypes.PING_SERVER_RESPONSE_ONLINE,
    payload: {
      server,
    },
  };
}

function initRebootServer(serverId) {
  return {
    type: actionTypes.REBOOT_SERVER_INIT,
    payload: {
      serverId,
    },
  };
}

function rebootServerResponseSuccess(server) {
  return {
    type: actionTypes.REBOOT_SERVER_RESPONSE_SUCCESS,
    payload: {
      server,
    }
  };
}

function rebootServerResponseError() {
  return {
    type: actionTypes.REBOOT_SERVER_RESPONSE_ERROR,
  };
}

export {
  initFetchServersAsync,
  initTurnOnServerAsync,
  initTurnOffServerAsync,
  initRebootServerAsync,
};