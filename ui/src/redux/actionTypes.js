const FETCH_SERVERS_INIT= 'FETCH_SERVERS_INIT';
const FETCH_SERVERS_RESPONSE_SUCCESS = 'FETCH_SERVERS_RESPONSE_SUCCESS';
const FETCH_SERVERS_RESPONSE_ERROR = 'FETCH_SERVERS_RESPONSE_ERROR';

const TURN_ON_SERVER_INIT = 'TURN_ON_SERVER';
const TURN_ON_SERVER_RESPONSE_SUCCESS = 'TURN_ON_SERVER_RESPONSE_SUCCESS';
const TURN_ON_SERVER_RESPONSE_ERROR = 'TURN_ON_SERVER_RESPONSE_ERROR';

const TURN_OFF_SERVER_INIT = 'TURN_OFF_SERVER_INIT';
const TURN_OFF_SERVER_RESPONSE_SUCCESS = 'TURN_OFF_SERVER_RESPONSE_SUCCESS';
const TURN_OFF_SERVER_RESPONSE_ERROR = 'TURN_OFF_SERVER_RESPONSE_ERROR';

const REBOOT_SERVER_INIT = 'REBOOT_SERVER';
const REBOOT_SERVER_RESPONSE_SUCCESS = 'REBOOT_SERVER_RESPONSE_SUCCESS';
const REBOOT_SERVER_RESPONSE_ERROR = 'REBOOT_SERVER_RESPONSE_ERROR';

const PING_SERVER_RESPONSE_ONLINE = 'PING_SERVER_RESPONSE_ONLINE';

export {
  FETCH_SERVERS_INIT,
  FETCH_SERVERS_RESPONSE_SUCCESS,
  FETCH_SERVERS_RESPONSE_ERROR,
  TURN_ON_SERVER_INIT,
  TURN_ON_SERVER_RESPONSE_SUCCESS,
  TURN_ON_SERVER_RESPONSE_ERROR,
  TURN_OFF_SERVER_INIT,
  TURN_OFF_SERVER_RESPONSE_SUCCESS,
  TURN_OFF_SERVER_RESPONSE_ERROR,
  REBOOT_SERVER_INIT,
  REBOOT_SERVER_RESPONSE_SUCCESS,
  REBOOT_SERVER_RESPONSE_ERROR,
  PING_SERVER_RESPONSE_ONLINE,
};