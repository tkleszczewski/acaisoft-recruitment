import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import serversReducer from './reducers/servers.reducer';

const store = createStore(
  serversReducer,
  applyMiddleware(thunkMiddleware)
);

export default store;
