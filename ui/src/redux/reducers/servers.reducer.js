import * as actionTypes from '../actionTypes';

const initialState = {
  loading: {
    allServers: false,
    serverId: null,
  },
  servers: [],
  errors: {
    fetchServers: false,
    turnOnServer: false,
    turnOffServer: false,
    rebootServer: false,
  }
};

const serversReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_SERVERS_INIT: {
      return {
        ...state,
        errors: {
          ...state.errors,
        },
        loading: {
          ...state.loading,
          allServers: true,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.FETCH_SERVERS_RESPONSE_SUCCESS: {
      return {
        ...state,
        errors: {
          ...state.errors,
          fetchServers: false,
        },
        loading: {
          ...state.loading,
          allServers: false,
        },
        servers: action.payload.servers,
      };
    }
    case actionTypes.FETCH_SERVERS_RESPONSE_ERROR: {
      return {
        ...state,
        errors: {
          ...state.errors,
          fetchServers: true,
        },
        loading: {
          ...state.loading,
          allServers: false,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.TURN_ON_SERVER_INIT: {
      return {
        ...state,
        errors: {
          ...state.errors,
        },
        loading: {
          ...state.loading,
          serverId: action.payload.serverId,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.TURN_ON_SERVER_RESPONSE_SUCCESS: {
      const servers = state.servers.map(server => ({ ...server }));

      const serverId = servers.findIndex(server => {
        return server.id === action.payload.server.id
      });
      servers.splice(serverId, 1, action.payload.server);

      return {
        ...state,
        errors: {
          ...state.errors,
          turnOnServer: false,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers,
      }
    }
    case actionTypes.TURN_ON_SERVER_RESPONSE_ERROR: {
      return {
        ...state,
        errors: {
          ...state.errors,
          turnOnServer: true,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers: state.servers.map(server => ({...server})),
      }
    }
    case actionTypes.TURN_OFF_SERVER_INIT: {
      return {
        ...state,
        errors: {
          ...state.errors,
        },
        loading: {
          ...state.loading,
          serverId: action.payload.serverId,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.TURN_OFF_SERVER_RESPONSE_SUCCESS: {
      const servers = state.servers.map(server => ({ ...server }));

      const serverId = servers.findIndex(server => {
        return server.id === action.payload.server.id
      });
      servers.splice(serverId, 1, action.payload.server);
      console.log(servers);

      return {
        ...state,
        errors: {
          ...state.errors,
          turnOffServer: false,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers,
      }
    }
    case actionTypes.TURN_OFF_SERVER_RESPONSE_ERROR: {
      return {
        ...state,
        errors: {
          ...state.errors,
          turnOffServer: true,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers: state.servers.map(server => ({...server})),
      }
    }
    case actionTypes.REBOOT_SERVER_INIT: {
      return {
        ...state,
        errors: {
          ...state.errors
        },
        loading: {
          ...state.loading,
          serverId: action.payload.serverId,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.REBOOT_SERVER_RESPONSE_SUCCESS: {
      const servers = state.servers.map(server => ({ ...server }));

      const serverId = servers.findIndex(server => {
        return server.id === action.payload.server.id
      });
      servers.splice(serverId, 1, action.payload.server);

      return {
        ...state,
        errors: {
          ...state.errors,
          rebootServer: false,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers,
      };
    }
    case actionTypes.REBOOT_SERVER_RESPONSE_ERROR: {
      return {
        ...state,
        errors: {
          ...state.errors,
          rebootServer: true,
        },
        loading: {
          ...state.loading,
          serverId: null,
        },
        servers: state.servers.map(server => ({...server})),
      };
    }
    case actionTypes.PING_SERVER_RESPONSE_ONLINE: {
      const servers = state.servers.map(server => ({ ...server }));

      const serverId = servers.findIndex(server => {
        return server.id === action.payload.server.id
      });
      servers.splice(serverId, 1, action.payload.server);

      return {
        ...state,
        errors: {
          ...state.errors,
        },
        loading: {
          ...state.loading,
        },
        servers,
      }
    }
    default: {
      return {
        ...state
      };
    }
  }
}

export default serversReducer;