import React from 'react';

import Table from '../Table/Table';

import classes from './MainContainer.module.css';

const MainContainer = () => (
  <div className={classes.MainContainer}>
    <Table/>
  </div>
);

export default MainContainer;