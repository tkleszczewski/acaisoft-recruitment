import React from 'react';

import classes from './TableHeader.module.css'

const TableHeader = () => {
  return (
    <div className={classes.TableHeader}>
      <div className={classes.TableHeaderCell}>NAME</div>
      <div className={classes.TableHeaderCell}>STATUS</div>
    </div>
  );
}

export default TableHeader;