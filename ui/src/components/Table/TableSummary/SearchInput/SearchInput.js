import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import classes from './SearchInput.module.css';

const SearchInput = (props) => {
  return (
    <div className={classes.SearchInputContainer}>
      <FontAwesomeIcon className={classes.SearchIcon} icon={faSearch}/>
      <input value={props.filterValue} onChange={props.onTextChange} className={classes.SearchInput} type="text" placeholder="Search"/>
    </div>
  );
}

export default SearchInput;