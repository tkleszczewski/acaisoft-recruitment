import React from 'react';

import SearchInput from './SearchInput/SearchInput';

import classes from './TableSummary.module.css';

const TableSummary = (props) => {
  return (
    <div className={classes.TableSummary}>
      <div className={classes.TitleAndCounter}>
        <h2 className={classes.Title}>Servers</h2>
        <p className={classes.Counter}>Number of elements: { props.serversNumber }</p>
      </div>
      <div>
        <SearchInput filterValue={props.filterValue} onTextChange={props.onTextChange}/>
      </div>
    </div>
  );
};

export default TableSummary;