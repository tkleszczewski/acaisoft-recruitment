import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import TableSummary from './TableSummary/TableSummary';
import TableHeader from './TableHeader/TableHeader';
import TableRow from './TableRow/TableRow';
import TableSpinner from './TableSpinner/TableSpinner';

import classes from './Table.module.css';

import * as actionCreators from '../../redux/actionCreators';

const Table = (props) => {

  useEffect(() => {
    props.fetchServers();
  }, []);

  const [srvFilterText, setSrvFilterText] = useState('');

  const rows = props.servers
    .filter(
      server => server.name.toLowerCase().indexOf(srvFilterText.toLowerCase()) !== -1
    )
    .map(
      server => (
        <TableRow server={server} key={server.id}/>
      )
    );

  const handleTextChange = (event) => {
    setSrvFilterText(event.target.value);
  }

  return (
    <div className={classes.TableContainer}>
      <TableSummary
        serversNumber={props.servers.length}
        filterValue={srvFilterText}
        onTextChange={handleTextChange}/>
      <div className={classes.Table}>
        <TableHeader/>
        <div className={classes.TableRows}>
          {
            props.requestFetchAllServersPending ? <TableSpinner/> : null
          }
          {
            rows
          }
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    servers: state.servers,
    requestFetchAllServersPending: state.loading.allServers,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchServers: () => dispatch(actionCreators.initFetchServersAsync()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);