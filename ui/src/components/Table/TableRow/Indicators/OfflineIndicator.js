import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import classes from './OfflineIndicator.module.css';

const OfflineIndicator = () => (
  <div className={classes.OfflineIndicator}>
    <FontAwesomeIcon className={classes.OfflineTimes} icon={faTimes}/>
    <p className={classes.OfflineText}>OFFLINE</p>
  </div>
);

export default OfflineIndicator;