import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

import classes from './OnlineIndicator.module.css';

const OnlineIndicator = () => (
  <div className={classes.OnlineIndicator}>
    <FontAwesomeIcon className={classes.OnlineCircle} icon={faCircle}/>
    <p className={classes.OnlineText}>ONLINE</p>
  </div>
);

export default OnlineIndicator;