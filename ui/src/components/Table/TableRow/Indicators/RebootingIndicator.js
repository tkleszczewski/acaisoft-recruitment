import React from 'react';

import classes from './RebootingIndicator.module.css';

const RebootingIndicator = () => (
  <div className={classes.RebootingIndicator}>
    <p className={classes.RebootingText}>REBOOTING...</p>
  </div>
);

export default RebootingIndicator;