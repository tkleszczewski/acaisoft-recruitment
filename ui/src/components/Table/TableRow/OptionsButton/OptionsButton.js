import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';

import classes from './OptionsButton.module.css';

const OptionsButton = (props) => {
  return (
    <div onClick={props.handleClick} className={classes.Options}>
      <FontAwesomeIcon className={classes.OptionsEllipsis} icon={faEllipsisH}/>
    </div>
  );
}

export default OptionsButton;