import React, { useState } from 'react';

import OfflineIndicator from './Indicators/OfflineIndicator';
import OnlineIndicator from './Indicators/OnlineIndicator';
import RebootingIndicator from './Indicators/RebootingIndicator';
import OptionsButton from './OptionsButton/OptionsButton';
import Dropdown from './Dropdown/Dropdown';

import classes from './TableRow.module.css';

const TableRow = (props) => {
  const [isDropdownVisible, setDropdownVisible] = useState(false);

  let indicator = null;

  switch(props.server.status) {
    case 'ONLINE': {
      indicator = <OnlineIndicator/>;
      break;
    }
    case 'OFFLINE': {
      indicator = <OfflineIndicator/>
      break
    }
    case 'REBOOTING': {
      indicator = <RebootingIndicator/>
      break;
    }
    default: {
      indicator = null;
      break;
    }
  }

  const toggleDropdown = () => {
    setDropdownVisible(isDropdownOpen => !isDropdownOpen);
  }

  const handleClickClose = () => {
    setDropdownVisible(false);
  }

  return (
    <div className={classes.TableRow}>
      <div className={classes.TableRowCellName}>
        { props.server.name }
      </div>
      <div className={classes.TableRowCellStatus}>
        { indicator }
      </div>
      <div className={classes.TableRowOptions}>
        <OptionsButton handleClick={toggleDropdown}/>
        {
          isDropdownVisible ? <Dropdown server={props.server} handleClickClose={handleClickClose}/> : null
        }
      </div>
    </div>
  );
}

export default TableRow;