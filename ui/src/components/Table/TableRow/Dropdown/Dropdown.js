import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux'

import classes from './Dropdown.module.css';

import * as actionCreators from '../../../../redux/actionCreators';

const Dropdown = (props) => {
  const dropdown = useRef(null);

  const handleClickClose = (event) => {
    if (dropdown && !dropdown.current.contains(event.target)) {
      props.handleClickClose();
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickClose);
  }, []);

  useEffect(() => {
    return () => {
      document.removeEventListener('click', handleClickClose);
    }
  }, []);

  let dropdownOptions = null;

  if (props.server.status === 'ONLINE') {
    dropdownOptions = (
      <React.Fragment>
        <div className={classes.DropdownOption} onClick={() => {props.turnOffServer(props.server.id); props.handleClickClose()}}>
          Turn off
        </div>
        <div className={classes.DropdownOption} onClick={() => {props.rebootServer(props.server.id); props.handleClickClose()}}>
          Reboot
        </div>
      </React.Fragment>
    );
  } else if (props.server.status === 'OFFLINE') {
    dropdownOptions = (
      <React.Fragment>
        <div className={classes.DropdownOption} onClick={() => {props.turnOnServer(props.server.id); props.handleClickClose()}}>
          Turn on
        </div>
      </React.Fragment>
    );
  } else {
    dropdownOptions = null;
  }

  return (
    <div ref={dropdown} className={classes.Dropdown}>
      {
        dropdownOptions
      }
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    turnOnServer: serverId => dispatch(actionCreators.initTurnOnServerAsync(serverId)),
    turnOffServer: serverId => dispatch(actionCreators.initTurnOffServerAsync(serverId)),
    rebootServer: serverId => dispatch(actionCreators.initRebootServerAsync(serverId)),
  };
}

export default connect(null, mapDispatchToProps)(Dropdown);