import React from 'react';

import classes from './TableSpinner.module.css';

const TableSpinner = () => {
  return (
    <div className={classes.TableSpinner}>
      <div className={classes.Circle}></div>
      <div className={classes.Rect}></div>
    </div>
  )
}

export default TableSpinner;