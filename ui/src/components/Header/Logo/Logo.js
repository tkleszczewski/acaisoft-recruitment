import React from 'react';

import LogoCircle from './LogoCircle/LogoCircle';
import LogoTitle from './LogoTitle/LogoTitle';
import LogoVerticalDivider from './LogoVerticalDivider/LogoVerticalDivider';

import classes from './Logo.module.css';

const Logo = () => (
  <div className={classes.Logo}>
    <LogoCircle/>
    <LogoTitle/>
    <LogoVerticalDivider/>
  </div>
)

export default Logo;