import React from 'react';

import classes from './LogoTitle.module.css';

const LogoTitle = () => (
  <h1 className={classes.LogoTitle}>Recruitment task</h1>
);

export default LogoTitle;