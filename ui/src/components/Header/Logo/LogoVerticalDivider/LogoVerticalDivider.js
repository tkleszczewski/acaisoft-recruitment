import React from 'react';

import classes from './LogoVerticalDivider.module.css';

const LogoVerticalDivider = () => (
  <div className={classes.LogoVerticalDivider}>
  </div>
);

export default LogoVerticalDivider;